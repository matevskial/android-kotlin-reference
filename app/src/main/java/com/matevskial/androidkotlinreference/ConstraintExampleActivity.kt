package com.matevskial.androidkotlinreference

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import java.util.*

/**
 * Activity for demonstrating constraint layout
 *
 * Contains simple button that generates the text that the TextView displays
 */
class ConstraintExampleActivity : AppCompatActivity() {

    private lateinit var generateButton: Button
    private lateinit var displayGeneratedText: TextView

    private var textCollection = arrayOf("Text 1", "Text 2", "Text 3")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_constraint_example)
        generateButton = findViewById(R.id.generateButton)
        displayGeneratedText = findViewById(R.id.displayGeneratedText)

        generateButton.setOnClickListener {
            displayGeneratedText.text = generateText()
        }
    }

    private fun generateText(): String {
        val idx = Random().nextInt(textCollection.size)
        return textCollection[idx]
    }
}